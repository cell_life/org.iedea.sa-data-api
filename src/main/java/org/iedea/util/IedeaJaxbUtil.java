package org.iedea.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

public class IedeaJaxbUtil {

	private static IedeaJaxbUtil instance;

	private JAXBContext context;
	
	private String packageName = "org.iedea";
	
	public static IedeaJaxbUtil instance() throws JAXBException {
		if (instance == null){
			instance = new IedeaJaxbUtil();
		}
		return instance;
	}
	
	private IedeaJaxbUtil() throws JAXBException {
		context = JAXBContext.newInstance(packageName, Thread
				.currentThread().getContextClassLoader());
	}
	
	public JAXBContext getContext() {
		return context;
	}
	
	public Object read(InputStream inputStream) throws JAXBException{
		InputStreamReader reader = null;
		try{
			reader = new InputStreamReader(inputStream);
			Unmarshaller unmarshaller = getContext().createUnmarshaller();
	
			Object element = unmarshaller.unmarshal(reader);
			return element;
		} finally {
			if (reader != null){
				try {
					reader.close();
				} catch (IOException ignore) {}
			}
		}
	}
	
	public void write(Object object, OutputStream out) throws JAXBException{
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(out);
			Marshaller m = getContext().createMarshaller();
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(object, writer);
		} catch (PropertyException e) {
			throw new JAXBException("Error parsing file",e);
		} finally {
			if (writer != null){
				try {
					writer.close();
				} catch (IOException ignore) {}
			}
		}
	}
}
